<?php
/**
 * Copyright 2015 SunnyDayInc, author: Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc;

use SunnyDayInc\Authentication\AccessToken;
use SunnyDayInc\Authentication\RefreshToken;
use SunnyDayInc\Authentication\OAuth2Client;
use SunnyDayInc\FileUpload\SunnyDayIncFile;
use SunnyDayInc\HttpClients\SunnyDayIncHttpClientInterface;
use SunnyDayInc\HttpClients\SunnyDayIncCurlHttpClient;
use SunnyDayInc\HttpClients\SunnyDayIncStreamHttpClient;
use SunnyDayInc\HttpClients\SunnyDayIncGuzzleHttpClient;
use SunnyDayInc\Exceptions\SunnyDayIncSDKException;

/**
 * Class SunnyDayInc
 *
 * @package SunnyDayInc
 */
class SunnyDayInc
{
    /**
     * @const string Version number of the SunnyDayInc PHP SDK.
     */
    const VERSION = '1.0.0';

    /**
     * @const string Default API version for requests.
     */
    const DEFAULT_API_VERSION = 'v1';

    /**
     * @const string The name of the environment variable that contains the app ID.
     */
    const APP_ID_ENV_NAME = 'SDI_APP_ID';

    /**
     * @const string The name of the environment variable that contains the app secret.
     */
    const APP_SECRET_ENV_NAME = 'SDI_APP_SECRET';

    /**
     * @var SunnyDayIncApp The SunnyDayIncApp entity.
     */
    protected $app;

    /**
     * @var SunnyDayIncClient The SunnyDayInc client service.
     */
    protected $client;

    /**
     * @var OAuth2Client The OAuth 2.0 client service.
     */
    protected $oAuth2Client;

    /**
     * @var AccessToken|null The default access token to use with requests.
     */
    protected $defaultAccessToken;

    /**
     * @var RefreshToken|null The default refresth token.
     */
    protected $defaultRefreshToken;

    /**
     * @var array|null The user data.
     */
    protected $user;

    /**
     * @var string|null The default API version we want to use.
     */
    protected $defaultApiVersion;

    /**
     * @var SunnyDayIncResponse|SunnyDayIncBatchResponse|null Stores the last request made to API.
     */
    protected $lastResponse;

    /**
     * Instantiates a new SunnyDayInc super-class object.
     *
     * @param array $config
     *
     * @throws SunnyDayIncSDKException
     */
    public function __construct(array $config = [])
    {
        $appId = isset($config['app_id']) ? $config['app_id'] : getenv(static::APP_ID_ENV_NAME);
        if (!$appId) {
            throw new SunnyDayIncSDKException('Required "app_id" key not supplied in config and could not find fallback environment variable "' . static::APP_ID_ENV_NAME . '"');
        }

        $appSecret = isset($config['app_secret']) ? $config['app_secret'] : getenv(static::APP_SECRET_ENV_NAME);
        if (!$appSecret) {
            throw new SunnyDayIncSDKException('Required "app_secret" key not supplied in config and could not find fallback environment variable "' . static::APP_SECRET_ENV_NAME . '"');
        }

        $baseUrlDev = isset($config['base_url_dev']) ? $config['base_url_dev'] : '';
        $certDev = isset($config['cert_dev']) ? $config['cert_dev'] : '';

        $this->app = new SunnyDayIncApp($appId, $appSecret);

        $enableBeta = isset($config['enable_beta_mode']) && $config['enable_beta_mode'] === true;

        $httpClientHandler = null;
        if (isset($config['http_client_handler'])) {
            if ($config['http_client_handler'] instanceof SunnyDayIncHttpClientInterface) {
                $httpClientHandler = $config['http_client_handler'];
            } elseif ($config['http_client_handler'] === 'curl') {
                $httpClientHandler = new SunnyDayIncCurlHttpClient(null, $enableBeta, $certDev);
            } elseif ($config['http_client_handler'] === 'stream') {
                $httpClientHandler = new SunnyDayIncStreamHttpClient(null, $enableBeta, $certDev);
            } elseif ($config['http_client_handler'] === 'guzzle') {
                $httpClientHandler = new SunnyDayIncGuzzleHttpClient(null, $enableBeta, $certDev);
            } else {
                throw new \InvalidArgumentException('The http_client_handler must be set to "curl", "stream", "guzzle", or be an instance of SunnyDayInc\HttpClients\SunnyDayIncHttpClientInterface');
            }
        }

        $this->client = new SunnyDayIncClient($httpClientHandler, $enableBeta, $baseUrlDev, $certDev);

        if (isset($config['default_access_token'])) {
            $this->setDefaultAccessToken($config['default_access_token']);
        }

        if (isset($config['default_refresh_token'])) {
            $this->setDefaultRefreshToken($config['default_refresh_token']);
        }

        if (isset($config['default_api_version'])) {
            $this->defaultApiVersion = $config['default_api_version'];
        } else {
            // @todo v6: Throw an InvalidArgumentException if "default_api_version" is not set
            $this->defaultApiVersion = static::DEFAULT_API_VERSION;
        }
    }

    /**
     * Returns the SunnyDayIncApp entity.
     *
     * @return SunnyDayIncApp
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * Returns the SunnyDayIncClient service.
     *
     * @return SunnyDayIncClient
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Returns the OAuth 2.0 client service.
     *
     * @return OAuth2Client
     */
    public function getOAuth2Client()
    {
        if (!$this->oAuth2Client instanceof OAuth2Client) {
            $app = $this->getApp();
            $client = $this->getClient();
            $this->oAuth2Client = new OAuth2Client($app, $client, $this->defaultApiVersion);
        }

        return $this->oAuth2Client;
    }

    /**
     * Returns the last response returned from API.
     *
     * @return SunnyDayIncResponse|SunnyDayIncBatchResponse|null
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * Returns the default AccessToken entity.
     *
     * @return AccessToken|null
     */
    public function getDefaultAccessToken()
    {
        return $this->defaultAccessToken;
    }

    /**
     * Returns the default RefreshToken entity.
     *
     * @return RefreshToken|null
     */
    public function getDefaultRefreshToken()
    {
        return $this->defaultRefreshToken;
    }

    /**
     * Returns the user.
     *
     * @return array|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets the default access token to use with requests.
     *
     * @param AccessToken|string $accessToken The access token to save.
     * @param int    $expiresAt
     *
     * @throws \InvalidArgumentException
     */
    public function setDefaultAccessToken($accessToken, $expiresAt = 0)
    {
        if (is_string($accessToken)) {
            $this->defaultAccessToken = new AccessToken($accessToken, $expiresAt);

            return;
        }

        if ($accessToken instanceof AccessToken) {
            $this->defaultAccessToken = $accessToken;

            return;
        }

        throw new \InvalidArgumentException('The default access token must be of type "string" or SunnyDayInc\AccessToken');
    }

    /**
     * Sets the default refresh token to use with requests.
     *
     * @param RefreshToken|string $refreshToken The access token to save.
     * @param int    $expiresAt
     *
     * @throws \InvalidArgumentException
     */
    public function setDefaultRefreshToken($refreshToken, $expiresAt = 0)
    {
        if (is_string($refreshToken)) {
            $this->defaultRefreshToken = new RefreshToken($refreshToken, $expiresAt);

            return;
        }

        if ($refreshToken instanceof RefreshToken) {
            $this->defaultRefreshToken = $refreshToken;

            return;
        }

        throw new \InvalidArgumentException('The default refresh token must be of type "string" or SunnyDayInc\RefreshToken');
    }

    /**
     * Returns the default API version.
     *
     * @return string
     */
    public function getDefaultApiVersion()
    {
        return $this->defaultApiVersion;
    }

    /**
     * Returns an AccessToken entity.
     *
     * @return AccessToken|null
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function getAccessToken()
    {
        if ($this->defaultAccessToken) {
            return $this->defaultAccessToken;
        }

        return $this->defaultAccessToken = $this->getOAuth2Client()->getAccessToken();
    }

    /**
     * Refresh the AccessToken and returns an AccessToken entity.
     *
     * @return AccessToken|null
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function refreshAccessToken()
    {
        if ( ! $this->defaultRefreshToken) {
            throw new \InvalidArgumentException('No RefreshToken found');
        }

        list($accessToken, $refreshToken) = $this->getOAuth2Client()->refreshAccessToken($this->defaultRefreshToken->getValue());

        $this->defaultAccessToken  = $accessToken;
        $this->defaultRefreshToken = $refreshToken;

        return $accessToken;
    }

    /**
     * Authenticate a user.
     *
     * @param string $email
     * @param string $password
     * @param array  $scope
     *
     * @return AccessToken|null
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function authenticateUser($email, $password, $scope = [])
    {
        if ( ! $this->defaultAccessToken) {
            throw new \InvalidArgumentException('No AccessToken found');
        }

        list($accessToken, $refreshToken, $user) = $this->getOAuth2Client()->authenticateUser(
            $email, 
            $password, 
            $this->defaultAccessToken->getValue(), 
            $scope
        );

        $this->defaultAccessToken  = $accessToken;
        $this->defaultRefreshToken = $refreshToken;
        $this->user                = $user;

        return $accessToken;
    }

    /**
     * Authenticate a user based on checkout identifier.
     *
     * @param string  $checkoutId
     * @param integer $userId
     * @param integer $sessionId
     * @param array   $scope
     *
     * @return AccessToken|null
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function authenticateUserByCheckoutIdentifier($checkoutId, $userId, $sessionId, $scope = [])
    {
        if ( ! $this->defaultAccessToken) {
            throw new \InvalidArgumentException('No AccessToken found');
        }

        list($accessToken, $refreshToken, $user) = $this->getOAuth2Client()->authenticateUserByCheckoutIdentifier(
            $checkoutId, 
            $userId, 
            $sessionId, 
            $this->defaultAccessToken->getValue(), 
            $scope
        );

        $this->defaultAccessToken  = $accessToken;
        $this->defaultRefreshToken = $refreshToken;
        $this->user                = $user;

        return $accessToken;
    }

    /**
     * Authenticate a user based on order identifier.
     *
     * @param integer $orderId
     * @param integer $customerId
     * @param array   $scope
     *
     * @return AccessToken|null
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function authenticateUserByOrderIdentifier($orderId, $customerId, $scope = [])
    {
        if ( ! $this->defaultAccessToken) {
            throw new \InvalidArgumentException('No AccessToken found');
        }

        list($accessToken, $refreshToken, $user) = $this->getOAuth2Client()->authenticateUserByOrderIdentifier(
            $orderId, 
            $customerId, 
            $this->defaultAccessToken->getValue(), 
            $scope
        );

        $this->defaultAccessToken  = $accessToken;
        $this->defaultRefreshToken = $refreshToken;
        $this->user                = $user;

        return $accessToken;
    }

    /**
     * Sends a GET request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function get($endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        return $this->sendRequest(
            'GET',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Sends a POST request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function post($endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        return $this->sendRequest(
            'POST',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Sends a PATCH request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function patch($endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        return $this->sendRequest(
            'PATCH',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Sends a DELETE request to API and returns the result.
     *
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function delete($endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        return $this->sendRequest(
            'DELETE',
            $endpoint,
            $params,
            $accessToken,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Sends a PUT request to API and returns the result.
     *
     * @param string                                   $endpoint
     * @param \SunnyDayInc\FileUpload\SunnyDayIncFile  $params
     * @param AccessToken|string|null                  $accessToken
     * @param string|null                              $eTag
     * @param string|null                              $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function put($endpoint, SunnyDayIncFile $file, $accessToken = null, $eTag = null, $apiVersion = null)
    {
        return $this->sendRequest(
            'PUT',
            $endpoint,
            ['file' => $file],
            $accessToken,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Sends a request to API and returns the result.
     *
     * @param string                  $method
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function sendRequest($method, $endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        $accessToken = $accessToken ?: $this->defaultAccessToken;
        $apiVersion = $apiVersion ?: $this->defaultApiVersion;
        $request = $this->request($method, $endpoint, $params, $accessToken, $eTag, $apiVersion);

        return $this->lastResponse = $this->client->sendRequest($request);
    }

    /**
     * Sends a batched request to API and returns the result.
     *
     * @param array                   $requests
     * @param AccessToken|string|null $accessToken
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncBatchResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function sendBatchRequest(array $requests, $accessToken = null, $apiVersion = null)
    {
        $accessToken = $accessToken ?: $this->defaultAccessToken;
        $apiVersion = $apiVersion ?: $this->defaultApiVersion;
        $batchRequest = new SunnyDayIncBatchRequest(
            $this->app,
            $requests,
            $accessToken,
            $apiVersion
        );

        return $this->lastResponse = $this->client->sendBatchRequest($batchRequest);
    }

    /**
     * Instantiates a new SunnyDayIncRequest entity.
     *
     * @param string                  $method
     * @param string                  $endpoint
     * @param array                   $params
     * @param AccessToken|string|null $accessToken
     * @param string|null             $eTag
     * @param string|null             $apiVersion
     *
     * @return SunnyDayIncRequest
     *
     * @throws SunnyDayIncSDKException
     */
    public function request($method, $endpoint, array $params = [], $accessToken = null, $eTag = null, $apiVersion = null)
    {
        $accessToken = $accessToken ?: $this->defaultAccessToken;
        $apiVersion = $apiVersion ?: $this->defaultApiVersion;

        return new SunnyDayIncRequest(
            $this->app,
            $accessToken,
            $method,
            $endpoint,
            $params,
            $eTag,
            $apiVersion
        );
    }

    /**
     * Factory to create SunnyDayIncFile's.
     *
     * @param string $pathToFile
     *
     * @return SunnyDayIncFile
     *
     * @throws SunnyDayIncSDKException
     */
    public function fileToUpload($pathToFile)
    {
        return new SunnyDayIncFile($pathToFile);
    }
}
