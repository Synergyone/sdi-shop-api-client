<?php
/**
 * Copyright 2015 Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc\Authentication;

use SunnyDayInc\SunnyDayInc;
use SunnyDayInc\SunnyDayIncApp;
use SunnyDayInc\SunnyDayIncRequest;
use SunnyDayInc\SunnyDayIncResponse;
use SunnyDayInc\SunnyDayIncClient;
use SunnyDayInc\Exceptions\SunnyDayIncResponseException;
use SunnyDayInc\Exceptions\SunnyDayIncSDKException;

/**
 * Class OAuth2Client
 *
 * @package SunnyDayInc
 */
class OAuth2Client
{
    /**
     * @const string The base authorization URL.
     */
    const BASE_AUTHORIZATION_URL = 'http://porter-api.sule.dev';

    /**
     * The SunnyDayIncApp entity.
     *
     * @var SunnyDayIncApp
     */
    protected $app;

    /**
     * The SunnyDayInc client.
     *
     * @var SunnyDayIncClient
     */
    protected $client;

    /**
     * The version of the API API to use.
     *
     * @var string
     */
    protected $apiVersion;

    /**
     * The last request sent to API.
     *
     * @var SunnyDayIncRequest|null
     */
    protected $lastRequest;

    /**
     * @param SunnyDayIncApp    $app
     * @param SunnyDayIncClient $client
     * @param string|null    $apiVersion The version of the API API to use.
     */
    public function __construct(SunnyDayIncApp $app, SunnyDayIncClient $client, $apiVersion = null)
    {
        $this->app = $app;
        $this->client = $client;
        $this->apiVersion = $apiVersion ?: SunnyDayInc::DEFAULT_API_VERSION;
    }

    /**
     * Returns the last SunnyDayIncRequest that was sent.
     * Useful for debugging and testing.
     *
     * @return SunnyDayIncRequest|null
     */
    public function getLastRequest()
    {
        return $this->lastRequest;
    }

    /**
     * Get the metadata associated with the access token.
     *
     * @param AccessToken|string $accessToken The access token to debug.
     *
     * @return AccessTokenMetadata
     */
    public function debugToken($accessToken)
    {
        $accessToken = $accessToken instanceof AccessToken ? $accessToken->getValue() : $accessToken;
        $params = ['input_token' => $accessToken];

        $this->lastRequest = new SunnyDayIncRequest(
            $this->app,
            $this->app->getAccessToken(),
            'GET',
            '/debug_token',
            $params,
            null,
            $this->apiVersion
        );
        $response = $this->client->sendRequest($this->lastRequest);
        $metadata = $response->getDecodedBody();

        return new AccessTokenMetadata($metadata);
    }

    /**
     * Generates an authorization URL to begin the process of authenticating a user.
     *
     * @param string $redirectUrl The callback URL to redirect to.
     * @param array  $scope       An array of permissions to request.
     * @param string $state       The CSPRNG-generated CSRF value.
     * @param array  $params      An array of parameters to generate URL.
     * @param string $separator   The separator to use in http_build_query().
     *
     * @return string
     */
    public function getAuthorizationUrl($redirectUrl, $state, array $scope = [], array $params = [], $separator = '&')
    {
        $params += [
            'client_id' => $this->app->getId(),
            'state' => $state,
            'response_type' => 'code',
            'sdk' => 'php-sdk-' . SunnyDayInc::VERSION,
            'redirect_uri' => $redirectUrl,
            'scope' => implode(',', $scope)
        ];

        return static::BASE_AUTHORIZATION_URL . '/oauth?' . http_build_query($params, null, $separator);
    }

    /**
     * Get a valid access token.
     *
     * @return AccessToken
     *
     * @throws SunnyDayIncSDKException
     */
    public function getAccessToken()
    {
        $params = [
            'grant_type' => 'client_credentials',
        ];

        return $this->requestAnAccessToken($params);
    }

    /**
     * Refresh the AccessToken and get a valid access token.
     *
     * @param string $refreshToken
     * 
     * @return array
     *
     * @throws SunnyDayIncSDKException
     */
    public function refreshAccessToken($refreshToken)
    {
        $params = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
        ];

        $response = $this->sendRequestWithClientParams('/oauth/access_token', $params, null, 'POST');
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new SunnyDayIncSDKException('Access token was not returned from API.', 401);
        }

        return [
            new AccessToken($data['access_token'], $data['expires']), 
            new RefreshToken($data['refresh_token'], $data['refresh_token_expires'])
        ];
    }

    /**
     * Authenticate a user and get a valid access token.
     *
     * @param string $email
     * @param string $password
     * @param string $accessToken
     * @param array  $scope
     *
     * @return array
     *
     * @throws SunnyDayIncSDKException
     */
    public function authenticateUser($email, $password, $accessToken, $scope = [])
    {
        $params = [
            'email'    => $email,
            'password' => $password,
            'scope'    => implode(' ', $scope),
        ];

        $response = $this->sendRequest('/authenticate', $params, $accessToken, 'POST');
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new SunnyDayIncSDKException('Access token was not returned from API.', 401);
        }

        $expiresAt = 0;
        if (isset($data['expires'])) {
            $expiresAt = $data['expires'];
        }

        $refreshExpiresAt = 0;
        if (isset($data['refresh_token_expires'])) {
            $refreshExpiresAt = $data['refresh_token_expires'];
        }

        return [
            new AccessToken($data['access_token'], $expiresAt), 
            new RefreshToken($data['refresh_token'], $refreshExpiresAt), 
            $data['user']
        ];
    }

    /**
     * Authenticate a user based on checkout identifier and get a valid access token.
     *
     * @param string  $checkoutId
     * @param integer $userId
     * @param integer $sessionId
     * @param string  $accessToken
     * @param array   $scope
     *
     * @return array
     *
     * @throws SunnyDayIncSDKException
     */
    public function authenticateUserByCheckoutIdentifier($checkoutId, $userId, $sessionId, $accessToken, $scope = [])
    {
        $params = [
            'user_id'    => $userId,
            'session_id' => $sessionId,
            'scope'      => implode(' ', $scope),
        ];

        $response = $this->sendRequest('/checkouts/'.$checkoutId.'/authenticate', $params, $accessToken, 'POST');
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new SunnyDayIncSDKException('Access token was not returned from API.', 401);
        }

        $expiresAt = 0;
        if (isset($data['expires'])) {
            $expiresAt = $data['expires'];
        }

        $refreshExpiresAt = 0;
        if (isset($data['refresh_token_expires'])) {
            $refreshExpiresAt = $data['refresh_token_expires'];
        }

        return [
            new AccessToken($data['access_token'], $expiresAt), 
            new RefreshToken($data['refresh_token'], $refreshExpiresAt), 
            $data['user']
        ];
    }

    /**
     * Authenticate a user based on order identifier and get a valid access token.
     *
     * @param integer $orderId
     * @param integer $customerId
     * @param string  $accessToken
     * @param array   $scope
     *
     * @return array
     *
     * @throws SunnyDayIncSDKException
     */
    public function authenticateUserByOrderIdentifier($orderId, $customerId, $accessToken, $scope = [])
    {
        $params = [
            'customer_id' => $customerId, 
            'scope'       => implode(' ', $scope),
        ];

        $response = $this->sendRequest('/orders/'.$orderId.'/authenticate', $params, $accessToken, 'POST');
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new SunnyDayIncSDKException('Access token was not returned from API.', 401);
        }

        $expiresAt = 0;
        if (isset($data['expires'])) {
            $expiresAt = $data['expires'];
        }

        $refreshExpiresAt = 0;
        if (isset($data['refresh_token_expires'])) {
            $refreshExpiresAt = $data['refresh_token_expires'];
        }

        return [
            new AccessToken($data['access_token'], $expiresAt), 
            new RefreshToken($data['refresh_token'], $refreshExpiresAt), 
            $data['user']
        ];
    }

    /**
     * Send a request to the OAuth endpoint.
     *
     * @param array $params
     *
     * @return AccessToken
     *
     * @throws SunnyDayIncSDKException
     */
    protected function requestAnAccessToken(array $params)
    {
        $response = $this->sendRequestWithClientParams('/oauth/access_token', $params, null, 'POST');
        $data = $response->getDecodedBody();

        if (!isset($data['access_token'])) {
            throw new SunnyDayIncSDKException('Access token was not returned from API.', 401);
        }

        $expiresAt = 0;
        if (isset($data['expires'])) {
            $expiresAt = $data['expires'];
        }

        return new AccessToken($data['access_token'], $expiresAt);
    }

    /**
     * Send a request to API with an app access token.
     *
     * @param string      $endpoint
     * @param array       $params
     * @param string|null $accessToken
     * @param string      $method
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncResponseException
     */
    protected function sendRequestWithClientParams($endpoint, array $params, $accessToken = null, $method = 'GET')
    {
        $params += $this->getClientParams();

        return $this->sendRequest($endpoint, $params, $accessToken, $method);
    }

    /**
     * Send a request to API with an app access token.
     *
     * @param string      $endpoint
     * @param array       $params
     * @param string|null $accessToken
     * @param string      $method
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncResponseException
     */
    protected function sendRequest($endpoint, array $params, $accessToken = null, $method = 'GET')
    {
        $accessToken = $accessToken ?: $this->app->getAccessToken();

        $this->lastRequest = new SunnyDayIncRequest(
            $this->app,
            $accessToken,
            $method,
            $endpoint,
            $params,
            null,
            $this->apiVersion
        );

        return $this->client->sendRequest($this->lastRequest);
    }

    /**
     * Returns the client_* params for OAuth requests.
     *
     * @return array
     */
    protected function getClientParams()
    {
        return [
            'client_id' => $this->app->getId(),
            'client_secret' => $this->app->getSecret(),
        ];
    }
}
