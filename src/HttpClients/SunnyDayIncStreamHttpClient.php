<?php
/**
 * Copyright 2015 Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc\HttpClients;

use SunnyDayInc\Http\RawResponse;
use SunnyDayInc\Exceptions\SunnyDayIncSDKException;

class SunnyDayIncStreamHttpClient implements SunnyDayIncHttpClientInterface
{
    /**
     * @var SunnyDayIncStream Procedural stream wrapper as object.
     */
    protected $sdiStream;

    /**
     * @var bool Toggle to use beta url.
     */
    protected $enableBetaMode = false;

    /**
     * @var string cert path development.
     */
    protected $certDev;

    /**
     * @param SunnyDayIncStream|null Procedural stream wrapper as object.
     * @param boolean $enableBeta
     * @param string $certDev
     */
    public function __construct(SunnyDayIncStream $sdiStream = null, $enableBeta = false, $certDev = '')
    {
        $this->sdiStream = $sdiStream ?: new SunnyDayIncStream();
        $this->enableBetaMode = $enableBeta;
        $this->certDev = $certDev;
    }

    /**
     * @inheritdoc
     */
    public function send($url, $method, $body, array $headers, $timeOut)
    {
        $options = [
            'http' => [
                'method' => $method,
                'header' => $this->compileHeader($headers),
                'content' => $body,
                'timeout' => $timeOut,
                'ignore_errors' => true
            ],
            'ssl' => [
                'verify_peer' => true,
                'verify_peer_name' => true,
                'allow_self_signed' => true, // All root certificates are self-signed
                'cafile' => ( ! empty($this->certDev)) ? $this->certDev : (__DIR__ . '/certs/DigiCertHighAssuranceEVRootCA' . (($this->enableBetaMode) ? '.beta' : '') . '.pem'),
            ],
        ];

        $this->sdiStream->streamContextCreate($options);
        $rawBody = $this->sdiStream->fileGetContents($url);
        $rawHeaders = $this->sdiStream->getResponseHeaders();

        if ($rawBody === false || !$rawHeaders) {
            throw new SunnyDayIncSDKException('Stream returned an empty response', 660);
        }

        $rawHeaders = implode("\r\n", $rawHeaders);

        return new RawResponse($rawHeaders, $rawBody);
    }

    /**
     * Formats the headers for use in the stream wrapper.
     *
     * @param array $headers The request headers.
     *
     * @return string
     */
    public function compileHeader(array $headers)
    {
        $header = [];
        foreach ($headers as $k => $v) {
            $header[] = $k . ': ' . $v;
        }

        return implode("\r\n", $header);
    }
}
