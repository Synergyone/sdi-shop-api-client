<?php
/**
 * Copyright 2015 Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc\HttpClients;

/**
 * Interface SunnyDayIncHttpClientInterface
 *
 * @package SunnyDayInc
 */
interface SunnyDayIncHttpClientInterface
{
    /**
     * Sends a request to the server and returns the raw response.
     *
     * @param string $url     The endpoint to send the request to.
     * @param string $method  The request method.
     * @param string $body    The body of the request.
     * @param array  $headers The request headers.
     * @param int    $timeOut The timeout in seconds for the request.
     *
     * @return \SunnyDayInc\Http\RawResponse Raw response from the server.
     *
     * @throws \SunnyDayInc\Exceptions\SunnyDayIncSDKException
     */
    public function send($url, $method, $body, array $headers, $timeOut);
}
