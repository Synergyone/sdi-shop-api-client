<?php
/**
 * Copyright 2015 Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc\HttpClients;

use SunnyDayInc\Http\RawResponse;
use SunnyDayInc\Exceptions\SunnyDayIncSDKException;

use GuzzleHttp\Client;
use GuzzleHttp\Message\ResponseInterface;
use GuzzleHttp\Ring\Exception\RingException;
use GuzzleHttp\Exception\RequestException;

class SunnyDayIncGuzzleHttpClient implements SunnyDayIncHttpClientInterface
{
    /**
     * @var \GuzzleHttp\Client The Guzzle client.
     */
    protected $guzzleClient;

    /**
     * @var bool Toggle to use beta url.
     */
    protected $enableBetaMode = false;

    /**
     * @var string cert path development.
     */
    protected $certDev;

    /**
     * @param \GuzzleHttp\Client|null The Guzzle client.
     * @param boolean $enableBeta
     * @param string $certDev
     */
    public function __construct(Client $guzzleClient = null, $enableBeta = false, $certDev = '')
    {
        $this->guzzleClient = $guzzleClient ?: new Client();
        $this->enableBetaMode = $enableBeta;
        $this->certDev = $certDev;
    }

    /**
     * @inheritdoc
     */
    public function send($url, $method, $body, array $headers, $timeOut)
    {
        $options = [
            'headers' => $headers,
            'body' => $body,
            'timeout' => $timeOut,
            'connect_timeout' => 10,
            'verify' => ( ! empty($this->certDev)) ? $this->certDev : (__DIR__ . '/certs/DigiCertHighAssuranceEVRootCA' . (($this->enableBetaMode) ? '.beta' : '') . '.pem'),
        ];
        $request = $this->guzzleClient->createRequest($method, $url, $options);

        try {
            $rawResponse = $this->guzzleClient->send($request);
        } catch (RequestException $e) {
            $rawResponse = $e->getResponse();

            if ($e->getPrevious() instanceof RingException || !$rawResponse instanceof ResponseInterface) {
                throw new SunnyDayIncSDKException($e->getMessage(), $e->getCode());
            }
        }

        $rawHeaders = $this->getHeadersAsString($rawResponse);
        $rawBody = $rawResponse->getBody();
        $httpStatusCode = $rawResponse->getStatusCode();

        return new RawResponse($rawHeaders, $rawBody, $httpStatusCode);
    }

    /**
     * Returns the Guzzle array of headers as a string.
     *
     * @param ResponseInterface $response The Guzzle response.
     *
     * @return string
     */
    public function getHeadersAsString(ResponseInterface $response)
    {
        $headers = $response->getHeaders();
        $rawHeaders = [];
        foreach ($headers as $name => $values) {
            $rawHeaders[] = $name . ": " . implode(", ", $values);
        }

        return implode("\r\n", $rawHeaders);
    }
}
