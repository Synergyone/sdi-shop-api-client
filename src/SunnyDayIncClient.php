<?php
/**
 * Copyright 2015 SunnyDayInc, author: Sulaeman <me@sulaeman.com>.
 *
 * You are hereby granted a non-exclusive, worldwide, royalty-free license to
 * use, copy, modify, and distribute this software in source code or binary
 * form for use in connection with the web services and APIs provided by
 * SunnyDayInc.
 *
 * As with any software that integrates with the SunnyDayInc platform, your use
 * of this software is subject to the SunnyDayInc Developer Principles and
 * Policies [http://developers.sunnydayinc.com/policy/]. This copyright notice
 * shall be included in all copies or substantial portions of the software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 */
namespace SunnyDayInc;

use SunnyDayInc\HttpClients\SunnyDayIncHttpClientInterface;
use SunnyDayInc\HttpClients\SunnyDayIncCurlHttpClient;
use SunnyDayInc\HttpClients\SunnyDayIncStreamHttpClient;
use SunnyDayInc\Exceptions\SunnyDayIncSDKException;

use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

/**
 * Class SunnyDayIncClient
 *
 * @package SunnyDayInc
 */
class SunnyDayIncClient
{
    /**
     * @const string Production API URL.
     */
    const BASE_API_URL = 'https://api.porter.id';

    /**
     * @const string Beta API URL.
     */
    const BASE_API_URL_BETA = 'https://api.sandbox.porter.id';

    /**
     * @const int The timeout in seconds for a normal request.
     */
    const DEFAULT_REQUEST_TIMEOUT = 60;

    /**
     * @var bool Toggle to use beta url.
     */
    protected $enableBetaMode = false;

    /**
     * @var string base URL development.
     */
    protected $baseUrlDev;

    /**
     * @var string cert path development.
     */
    protected $certDev;

    /**
     * @var SunnyDayIncHttpClientInterface HTTP client handler.
     */
    protected $httpClientHandler;

    /**
     * @var int The number of calls that have been made to.
     */
    public static $requestCount = 0;

    /**
     * Instantiates a new SunnyDayIncClient object.
     *
     * @param SunnyDayIncHttpClientInterface|null $httpClientHandler
     * @param boolean                             $enableBeta
     * @param string                              $baseUrlDev
     * @param string                              $certDev
     */
    public function __construct(SunnyDayIncHttpClientInterface $httpClientHandler = null, $enableBeta = false, $baseUrlDev = '', $certDev = '')
    {
        $this->enableBetaMode = $enableBeta;
        $this->certDev = $certDev;
        $this->httpClientHandler = $httpClientHandler ?: $this->detectHttpClientHandler();
        $this->baseUrlDev = $baseUrlDev;
    }

    /**
     * Sets the HTTP client handler.
     *
     * @param SunnyDayIncHttpClientInterface $httpClientHandler
     */
    public function setHttpClientHandler(SunnyDayIncHttpClientInterface $httpClientHandler)
    {
        $this->httpClientHandler = $httpClientHandler;
    }

    /**
     * Returns the HTTP client handler.
     *
     * @return SunnyDayIncHttpClientInterface
     */
    public function getHttpClientHandler()
    {
        return $this->httpClientHandler;
    }

    /**
     * Detects which HTTP client handler to use.
     *
     * @return SunnyDayIncHttpClientInterface
     */
    public function detectHttpClientHandler()
    {
        return function_exists('curl_init') ? new SunnyDayIncCurlHttpClient(null, $this->enableBetaMode, $this->certDev) : new SunnyDayIncStreamHttpClient(null, $this->enableBetaMode, $this->certDev);
    }

    /**
     * Toggle beta mode.
     *
     * @param boolean $betaMode
     */
    public function enableBetaMode($betaMode = true)
    {
        $this->enableBetaMode = $betaMode;
    }

    /**
     * Returns the base URL.
     *
     * @return string
     */
    public function getBaseApiUrl()
    {
        if ( ! empty($this->baseUrlDev)) {
            return $this->baseUrlDev;
        }
        
        return $this->enableBetaMode ? static::BASE_API_URL_BETA : static::BASE_API_URL;
    }

    /**
     * Prepares the request for sending to the client handler.
     *
     * @param SunnyDayIncRequest $request
     *
     * @return array
     */
    public function prepareRequestMessage(SunnyDayIncRequest $request)
    {
        $url = $this->getBaseApiUrl() . $request->getUrl();

        if ($request->getMethod() !== 'PUT') {
            // If we're sending files they should be sent as multipart/form-data
            if ($request->containsFileUploads()) {
                $requestBody = $request->getMultipartBody();
                $request->setHeaders([
                    'Content-Type' => 'multipart/form-data; boundary=' . $requestBody->getBoundary(),
                ]);
            } else {
                $requestBody = $request->getUrlEncodedBody();
                $request->setHeaders([
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ]);
            }

            $body = $requestBody->getBody();
        } else {
            $guesser = ExtensionGuesser::getInstance();

            $body = $request->getFile()->getContents();
            $request->setHeaders([
                'Content-Type' => $request->getFile()->getMimetype(),
                'Content-Length' => $request->getFile()->getSize(),
                'X-Content-Extension' => $guesser->guess($request->getFile()->getMimetype())
            ]);

            unset($guesser);
        }

        return [
            $url,
            $request->getMethod(),
            $request->getHeaders(),
            $body
        ];
    }

    /**
     * Makes the request to and returns the result.
     *
     * @param SunnyDayIncRequest $request
     *
     * @return SunnyDayIncResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function sendRequest(SunnyDayIncRequest $request)
    {
        if (get_class($request) === 'SunnyDayIncRequest') {
            $request->validateAccessToken();
        }

        list($url, $method, $headers, $body) = $this->prepareRequestMessage($request);

        // Since file uploads can take a while, we need to give more time for uploads
        $timeOut = static::DEFAULT_REQUEST_TIMEOUT;
        
        // Should throw `SunnyDayIncSDKException` exception on HTTP client error.
        // Don't catch to allow it to bubble up.
        $rawResponse = $this->httpClientHandler->send($url, $method, $body, $headers, $timeOut);

        static::$requestCount++;

        $returnResponse = new SunnyDayIncResponse(
            $request,
            $rawResponse->getBody(),
            $rawResponse->getHttpResponseCode(),
            $rawResponse->getHeaders()
        );

        if ($returnResponse->isError()) {
            throw $returnResponse->getThrownException();
        }

        return $returnResponse;
    }

    /**
     * Makes a batched request to and returns the result.
     *
     * @param SunnyDayIncBatchRequest $request
     *
     * @return SunnyDayIncBatchResponse
     *
     * @throws SunnyDayIncSDKException
     */
    public function sendBatchRequest(SunnyDayIncBatchRequest $request)
    {
        $request->prepareRequestsForBatch();
        $sunnydayincResponse = $this->sendRequest($request);

        return new SunnyDayIncBatchResponse($request, $sunnydayincResponse);
    }
}
